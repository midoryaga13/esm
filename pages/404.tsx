
export default function Custom404() {
    return <div className="bg-black container self-center text-xl text-white pt-64 pl-72">
        <h1>404 - Page Not Found</h1>
    </div>

}