import type { NextPage } from 'next'
import { Provider } from 'react-redux'
import Layout from '../src/components/Layout'
import store from '../src/store'
import useTranslation from 'next-translate/useTranslation';

const Home: NextPage = () => {
  const { t } = useTranslation("common");
  return (
    <Provider store={store}>
      <Layout title="Home">
        <h1>Hello Next.js 👋</h1>
        <h1>{t('hello')}👋</h1>
      </Layout>
    </Provider>
  )
}

export default Home
