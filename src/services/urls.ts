
export const prefixer = process.env.NEXT_PUBLIC_REACT_APP_API_URL;
export const UserUrl = {
    FETCH_DEFAULT_DATA: `${prefixer}user/default_data`,
    GET_USER_ME: `${prefixer}user/me`,
};