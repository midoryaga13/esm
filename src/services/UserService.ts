import BaseService from "./BaseService";
import { UserUrl } from "./urls";

class UserService {
  static getDefaultData = () =>
    BaseService.getRequest(UserUrl.FETCH_DEFAULT_DATA, false);
}

export default UserService;
