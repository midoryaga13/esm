import { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import Link from 'next/link'
import Head from 'next/head'
import { loadGlobalDataEffect } from '../store/effects'
import SwitchLanguage from './core/SwitchLang'

interface LayoutProps {
    title?: string,
    children: React.ReactNode;
}
const layoutStyle = {
    margin: 20,
    padding: 20,
    border: '1px solid #DDD'
}
const Layout = (props: LayoutProps) => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(loadGlobalDataEffect());
    }, [])
    return (
        <div className='m-20 p-20 clsx'>
            <Head>
                <title>{props.title}</title>
                <meta charSet="utf-8" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            {/* <script src="../path/to/@themesberg/flowbite/dist/flowbite.bundle.js"></script> */}
            <header>
                <nav>
                    <Link href="/">
                        <a>Home</a>
                    </Link>{' '}
                    |{' '}
                    <Link href="/about">
                        <a>About</a>
                    </Link>{' '}
                    |{' '}
                    <Link href="/resource">
                        <a>About</a>
                    </Link>{' '}
                    |{' '}
                </nav>
            </header>
            <body>
                <SwitchLanguage />
                {props.children}
            </body>
        </div>
    );
}

export default Layout;
