import React, { useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import i18nConfig from '../../../i18n.json';
import Switch from '@mui/material/Switch';

const SwitchLanguage = () => {
    const { locales, defaultLocale } = i18nConfig;

    const [toggle, setToggle] = useState(true);


    const handleChange = (event: any) => {
        console.log("change mode");
        setToggle(!toggle);
    };

    return (
        <div className='flex items-center'>
            <Switch
                checked={toggle}
                // onChange={}
                inputProps={{ 'aria-label': 'controlled' }}
                onClick={(e) => handleChange(e)
                }
            />
        </div>
    );
};

export default SwitchLanguage;