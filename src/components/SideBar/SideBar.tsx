import { useEffect, useState } from "react";
import { Box, Drawer, Hidden } from '@material-ui/core';
import { makeStyles } from "@material-ui/styles";
import { Theme } from "@mui/material";

interface Path {
    pathName: string;
    pathStatus: boolean
}

const useStyles = makeStyles((theme: Theme) => ({

    mobileDrawer: {
        width: 256
    }
}));

const SideBar = () => {

    const classes = useStyles();

    return <>
        <Hidden>
            <Drawer
                anchor="left"
                classes={{ paper: classes.mobileDrawer }}
            >
            </Drawer>
        </Hidden>
    </>
};

export default SideBar