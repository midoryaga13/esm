import { useRouter } from "next/router";
import { Route, BrowserRouter, Routes } from "react-router-dom"
import Result from "./Result";
import StepOne from "./StepOne";
import StepThree from "./StepThree";
import StepTwo from "./StepTwo";
const Form = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<StepOne />} />
                <Route path="/step2" element={<StepTwo />} />
                <Route path="/step3" element={<StepThree />} />
                <Route path="/result" element={<Result />} />
            </Routes>
        </BrowserRouter>
    );
};

export default Form;