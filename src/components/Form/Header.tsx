import React from 'react'
import { Theme, Typography } from '@mui/material';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        margin: theme.spacing(3, 0, 2),
        fontFamily: "Permanent Marker",
        textAling: "center",
        fontSize: "40px",
        color: "deeppink",
        textShadow: "1px 1px darkmagenta"
    }
}));

const Header = () => {
    const styles = useStyles();
    return (
        <Typography className={styles.root} component="h1">
            The StepForm
        </Typography>
    )
};