import { applyMiddleware, createStore, compose, Store } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { ApplicationState } from './types';
import rootReducer from './reducers';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from "redux-logger"

const loggerMiddleware = createLogger();

const middlewares = [];

middlewares.push(thunkMiddleware);
if (process.env.NEXT_PUBLIC_ENV === 'DEV') {
  middlewares.push(loggerMiddleware);
}

const middlewareEnhancer = composeWithDevTools(applyMiddleware(...middlewares));

const enhancers = [middlewareEnhancer];
const composedEnhancers = compose(...enhancers);

const store: Store<ApplicationState> = createStore(
  rootReducer,
  //@ts-ignore
  composedEnhancers
);

export default store;
