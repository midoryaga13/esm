import { Action } from "redux"

export const SET_GLOBAL_DATA_ACTION = "SET_GLOBAL_DATA_ACTIONt"

export interface SetGlobalData extends Action {
    type: typeof SET_GLOBAL_DATA_ACTION;
    data: GlobalDataState;
}

export interface GlobalDataState {
    user_role: Area[];
    areas: Area[];
    user_sex: Area[];
    menu_type: Area[];
    days: Area[];
    event_types: Area[];
    location_types: Area[];
    service_types: Area[];
    booking_status: string[];
    booking_type: string[];
    payment_type: string[];
}

export interface Area {
    id: number;
    title_fr: string;
    title_en: string;
}

// Converts JSON strings to/from your types
export class Convert {
    public static toMenuList(json: string): GlobalDataState {
        return JSON.parse(json);
    }

    public static menuListToJson(value: GlobalDataState): string {
        return JSON.stringify(value);
    }
}
