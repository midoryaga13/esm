
import { ThunkAction } from 'redux-thunk';
import UserService from '../../services/UserService';
import { setGlobalData } from '../actions';
import { ApplicationAction, ApplicationState } from '../types';




type Effect = ThunkAction<any, ApplicationState, any, ApplicationAction>; //! write documentation here for this line

export const loadGlobalDataEffect = (): Effect => async (dispatch) => {
    return UserService.getDefaultData().then(async (response: any) => {
        console.log("response : ", response);

        if (response.status === 200) {
            dispatch(setGlobalData(await response.json()));
        }
    });
};

