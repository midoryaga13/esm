import { combineReducers, Reducer } from "redux";
import GlobalDataReducer from "./reducers/GlobalDataReducer";
import { ApplicationState } from "./types";


const rootReducer: Reducer<ApplicationState> =
  combineReducers<ApplicationState>({
    // @ts-ignore
    data: GlobalDataReducer,
  });

export default rootReducer;