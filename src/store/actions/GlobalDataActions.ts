import { GlobalDataState, SetGlobalData, SET_GLOBAL_DATA_ACTION } from "../types";

export const setGlobalData = (data: GlobalDataState): SetGlobalData => ({
  type: SET_GLOBAL_DATA_ACTION,
  data: data
});
