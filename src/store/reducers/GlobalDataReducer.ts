import { ApplicationAction, GlobalDataState, SET_GLOBAL_DATA_ACTION } from "../types";


const initialState: GlobalDataState = {
  user_role: [],
  areas: [],
  user_sex: [],
  menu_type: [],
  days: [],
  event_types: [],
  location_types: [],
  service_types: [],
  booking_status: [],
  booking_type: [],
  payment_type: []
};

const reducer = (state = initialState, action: ApplicationAction) => {
  switch (action.type) {
    case SET_GLOBAL_DATA_ACTION: {
      return action.data;
    }
    default: {
      return state;
    }
  }
};

export default reducer;