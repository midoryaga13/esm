import { GlobalDataState, SetGlobalData } from "./types/GlobalDataTypes";

export * from "./types/GlobalDataTypes";


export interface ApplicationState {
  data: GlobalDataState;
}

export type ApplicationAction =
  | SetGlobalData